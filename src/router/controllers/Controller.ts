import express = require('express');

abstract class Controller<T> {
    protected router: express.Application;

    protected constructor(router: express.Application) {
        this.router = router;

        this.registerDefaultRoutes();
        this.registerRoutes();
    }

    private registerDefaultRoutes(): void {
        this.router.get('/', (req: express.Request, res: express.Response) => {
            res.status(200).send('Caprarelli means freschezza');
        });

        this.router.get('/ping', (req: express.Request, res: express.Response) => {
            res.status(200).send();
        });
    }

    abstract registerRoutes(): void;
}

export default Controller;
