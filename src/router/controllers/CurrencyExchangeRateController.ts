import express = require('express');
import Controller from "./Controller";
import CurrencyExchangeRate from "../../database/models/CurrencyExchangeRate";
import CurrencyExchangeRateService from "../services/CurrencyExchangeRateService";
import CurrencyService from "../services/CurrencyService";

class CurrencyExchangeRateController extends Controller<CurrencyExchangeRate> {
    private currencyService: CurrencyService;
    private currencyExchangeRateService: CurrencyExchangeRateService;

    constructor(router: express.Application) {
        super(router);

        this.currencyExchangeRateService = new CurrencyExchangeRateService();
        this.currencyService = new CurrencyService();
    }

    registerRoutes(): void {
        this.router.get('/exchange-rates', this.getAll.bind(this));
        this.router.get('/exchange-rates/:id', this.getById.bind(this));
        this.router.post('/exchange-rates', this.createExchangeRate.bind(this));
        this.router.put('/exchange-rates/:id', this.updateExchangeRate.bind(this));
        this.router.delete('/exchange-rates/:id', this.deleteExchangeRate.bind(this));
        this.router.get('/convert-currency', this.convertCurrency.bind(this));
    }

    getAll(request: express.Request, response: express.Response): void {
        this.currencyExchangeRateService.findAll().then((models) => {
            response.status(200).send(models);
        });
    }

    getById(request: express.Request, response: express.Response): void {
        const {id} = request.params;
        if (!id) {
            response.status(400).send("Missing ID parameter");
        }

        this.currencyExchangeRateService.findById(id).then((model) => {
            if (!model) {
                response.sendStatus(404);
            }

            response.status(200).send(model);
        });
    }

    createExchangeRate(request: express.Request, response: express.Response): void {
        const {body} = request;
        if (!body.fromCode) {
            response.status(400).send('Missing `fromCode` value');
        }
        if (!body.toCode) {
            response.status(400).send('Missing `toCode` value');
        }
        if (!body.conversionRate) {
            response.status(400).send('Missing `conversionRate` value');
        }

        this.currencyExchangeRateService.create(body.fromCode, body.toCode, body.conversionRate).then((model) => {
            response.status(201).send(model);
        })
    }

    updateExchangeRate(request: express.Request, response: express.Response): void {
    }

    deleteExchangeRate(request: express.Request, response: express.Response): void {
    }

    convertCurrency(request: express.Request, response: express.Response): void {

    }
}

export default CurrencyExchangeRateController;

