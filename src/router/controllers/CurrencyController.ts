import express = require('express');
import Controller from "./Controller";
import Currency from "../../database/models/Currency";
import CurrencyService from "../services/CurrencyService";

class CurrencyController extends Controller<Currency> {
    private currencyService: CurrencyService;

    constructor(router: express.Application) {
        super(router);

        this.currencyService = new CurrencyService();
    }

    registerRoutes(): void {
        this.router.get('/currencies', this.getAll.bind(this));
        this.router.get('/currencies/:id', this.getById.bind(this));
        this.router.post('/currencies', this.createCurrency.bind(this));
        this.router.put('/currencies/:id', this.updateCurrency.bind(this));
        this.router.delete('/currencies/:id', this.deleteCurrency.bind(this));
    }

    getAll(request: express.Request, response: express.Response): void {
        this.currencyService.findAll().then((models) => {
            response.status(200).send(models);
        });
    }

    getById(request: express.Request, response: express.Response): void {
        const {id} = request.params;
        if (!id) {
            response.status(400).send('Missing ID from request')
        }

        this.currencyService.findById(id).then((model) => {
            response.status(200).send(model);
        });
    }

    createCurrency(request: express.Request, response: express.Response): void {
        const {body} = request;
        this.currencyService.create(body.code, body.name).then((model) => {
            response.status(201).send(model);
        });
    }

    updateCurrency(request: express.Request, response: express.Response): void {
        const {id} = request.params;
        if (!id) {
            response.status(400).send('Missing ID from request')
        }
        const {body} = request;
        if (!body.name) {
            response.status(400).send('Missing name from request')
        }
        if (!body.code) {
            response.status(400).send('Missing code from request')
        }

        this.currencyService.update(id, body.code, body.name).then((model) => {
            response.status(202).send(model);
        });
    }

    deleteCurrency(request: express.Request, response: express.Response): void {
        const {id} = request.params;
        if (!id) {
            response.status(400).send('Missing ID from request')
        }

        this.currencyService.delete(id).then((result) => {
            response.status(202).send(result);
        });
    }
}

export default CurrencyController;

