import express = require('express');
import CurrencyController from "./controllers/CurrencyController";
import CurrencyExchangeRateController from "./controllers/CurrencyExchangeRateController";

class RouterManager {
    private readonly router: express.Application;
    private readonly port: number;

    constructor() {
        this.router = express();
        this.router.use(express.json());

        const {PORT = "3000"} = process.env;
        this.port = parseInt(PORT);

        new CurrencyController(this.router);
        new CurrencyExchangeRateController(this.router);
    }

    public getPort(): number {
        return this.port;
    }

    listen(callback: () => void): void {
        this.router.listen(this.port, callback);
    }
}

export default RouterManager;
