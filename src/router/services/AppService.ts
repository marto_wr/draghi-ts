import {Connection, getConnection, ObjectType, Repository} from "typeorm";

abstract class AppService {
    private connection: Connection;

    protected constructor() {
        this.connection = getConnection();
    }

    protected getRepository<T>(repository: ObjectType<Repository<T>>) {
        return this.connection.getCustomRepository(repository);
    }
}

export default AppService;
