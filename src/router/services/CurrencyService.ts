import AppService from "./AppService";
import Currency from "../../database/models/Currency";
import CurrencyRepository from "../../database/repositories/CurrencyRepository";
import {DeleteResult, Repository} from "typeorm";

class CurrencyService extends AppService {
    private currencyRepository: Repository<Currency>;

    constructor() {
        super();

        this.currencyRepository = super.getRepository(CurrencyRepository);
    }

    public findAll(params?: object): Promise<Currency[]> {
        return this.currencyRepository.find(params);
    }

    public findById(id: string): Promise<Currency | undefined> {
        return this.currencyRepository.findOne({
            where: {
                id: parseInt(id)
            }
        })
    }

    public findByCode(code: string): Promise<Currency | undefined> {
        return this.currencyRepository.findOne({
            where: {code}
        })
    }

    public create(code: string, name: string): Promise<Currency> {
        const currency = new Currency();
        currency.code = code;
        currency.name = name;

        return this.currencyRepository.save(currency);
    }

    public update(id: string, code: string, name: string): Promise<Currency> {
        return this.findById(id).then((model) => {
            if (!model) {
                throw new Error('Not found')
            } else {
                model.code = code;
                model.name = name;

                return this.currencyRepository.save(model);
            }
        })
    }

    public delete(id: string): Promise<DeleteResult> {
        return this.findById(id).then((model) => {
            if (!model) {
                throw new Error('Not found')
            } else {
                return this.currencyRepository.delete(model);
            }
        })
    }
}

export default CurrencyService;
