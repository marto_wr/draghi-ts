import AppService from "./AppService";
import CurrencyExchangeRateRepository from "../../database/repositories/CurrencyExchangeRateRepository";
import {Repository} from "typeorm";
import CurrencyExchangeRate from "../../database/models/CurrencyExchangeRate";
import CurrencyService from "./CurrencyService";

class CurrencyExchangeRateService extends AppService {
    private currencyExchangeRateRepository: Repository<CurrencyExchangeRate>;
    private currencyService: CurrencyService;

    constructor() {
        super();

        this.currencyExchangeRateRepository = super.getRepository(CurrencyExchangeRateRepository);
        this.currencyService = new CurrencyService();
    }

    public findAll(params?: object): Promise<CurrencyExchangeRate[]> {
        return this.currencyExchangeRateRepository.find({
            ...params,
            relations: ["toCurrency", "fromCurrency"]
        });
    }

    public findById(id: string): Promise<CurrencyExchangeRate | undefined> {
        return this.currencyExchangeRateRepository.findOne({
            where: {
                id: parseInt(id)
            },
            relations: ["toCurrency", "fromCurrency"]
        })
    }

    public create(fromCode: string, toCode: string, conversionRate: number): Promise<CurrencyExchangeRate> {
        return Promise.all([
            this.currencyService.findByCode(fromCode),
            this.currencyService.findByCode(toCode)
        ]).then((currencies) => {
            const [fromCurrency, toCurrency] = currencies;
            if (!fromCurrency || !toCurrency) {
                throw new Error('Currencies not found');
            }
            const currencyExchangeRate = new CurrencyExchangeRate();
            currencyExchangeRate.fromCurrency = fromCurrency;
            currencyExchangeRate.toCurrency = toCurrency;
            currencyExchangeRate.exchangeRate = conversionRate;

            return this.currencyExchangeRateRepository.save(currencyExchangeRate);
        });
    }

    public convertCurrency(fromCode: string, toCode: string) {

    }
}

export default CurrencyExchangeRateService;
