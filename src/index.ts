import * as dotenv from 'dotenv';
import ConnectionManager from "./database/ConnectionManager";
import RouterManager from "./router/RouterManager";
import {Connection} from "typeorm";

dotenv.config();

const connectionManager = new ConnectionManager();
connectionManager.connect().then((connection: Connection) => {
    const routerManager = new RouterManager();
    routerManager.listen(() => {
        console.log(`Router listening on port: ${routerManager.getPort()}`)
    })
});

