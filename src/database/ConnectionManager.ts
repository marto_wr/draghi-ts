import {Connection, ConnectionOptions, createConnection} from "typeorm";

class ConnectionManager {
    private readonly connectionOptions: ConnectionOptions;

    constructor() {
        this.connectionOptions = {
            name: "default",
            // @ts-ignore
            type: process.env.DB_DRIVER,
            database: process.env.DB_NAME,
            synchronize: true,
            logger: "advanced-console",
            entities: [
                `${__dirname}/models/*.js`
            ]
        };
    }

    public connect(): Promise<Connection> {
        return createConnection(this.connectionOptions);
    }
}

export default ConnectionManager;
