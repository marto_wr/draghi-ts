import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import Currency from "./Currency";

@Entity()
class CurrencyExchangeRate {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => Currency)
    @JoinColumn()
    fromCurrency: Currency;

    @OneToOne(type => Currency)
    @JoinColumn()
    toCurrency: Currency;

    @Column("decimal", {precision: 10, scale: 5})
    exchangeRate: number;
}

export default CurrencyExchangeRate;
