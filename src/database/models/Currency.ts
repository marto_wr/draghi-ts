import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
class Currency {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({
        length: 3
    })
    code: string;
}

export default Currency;
