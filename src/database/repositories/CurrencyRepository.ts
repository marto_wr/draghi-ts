import Currency from "../models/Currency";
import {EntityRepository, Repository} from "typeorm";

@EntityRepository(Currency)
class CurrencyRepository extends Repository<Currency> {
    public findByCode(code: string): Promise<Currency | undefined> {
        return this.findOne({
            where: {code}
        })
    }

    public findByName(name: string): Promise<Currency | undefined> {
        return this.findOne({
            where: {name}
        })
    }
}

export default CurrencyRepository;
