import {EntityRepository, Repository} from "typeorm";
import CurrencyExchangeRate from "../models/CurrencyExchangeRate";

@EntityRepository(CurrencyExchangeRate)
class CurrencyExchangeRateRepository extends Repository<CurrencyExchangeRate> {

}

export default CurrencyExchangeRateRepository;
